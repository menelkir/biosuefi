<b><font=4>Contents:</font></b><br>
<b>Original</b> - Original v0304R BIOS File<br>
<b>Patched/b> - Patched v0304R BIOS File using UBU

----

<B>Patches Used</B>:<br>
Intel RST(e) OROM and EFI Sata Driver v14.8.2.2397<br>
Intel OROM VBIOS SNB-IVB v2132<br>
EFI GOP Driver Ivy Bridge v3.0.1030<br>
EFI GOP Driver Sandy Bridge v2.0.1024 <br>
OROM Intel Boot Agent GE v1.5.62	<br>
EFI Intel PRO/1000 UNDI v6.6.04<br>
Ivy Bridge CPUID 0306A9 v1C 02-26-2015<br>
Sandy Bridge CPUID 0206A7 v29 06-12-2013<br>


