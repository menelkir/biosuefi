BIOS Mod Documentation
(VERSION: TZ1H_290)
(DATE: June 16, 2014)

�No SLIC MODS WERE MADE TO THIS IMAGE
�Only the TEMPLAT00.ROM module was modified (modifications are listed below)
�Modded with HxD Hex Editor
�Unpacking/Repacking accomplished with AndyP's PhoenixTool (Version 2.54)
�Verified using HxD and Winmerge (PBE was not used for verification as the image was too new - PSI Analysis Fail)

-------------------------------------------------------------------------------------------------------------
Changelog:

NEW PARENT MENUS:

�INTEL Menu

-------------------------------------------------------------------------------------------------------------
TZ1H_290.rar Contents:


TZ1H_290_mod.wph: This is the modified BIOS image. It was created using the original "TZ1H_290.wph" image.

SLIC.log: This is the log file of the unpacking and repacking processes (PhoenixTool 2.54).

TEMPLAT00.ROM: This is the original unmodified TEMPLAT module.

TEMPLAT00_mod.ROM: This is the modified TEMPLAT module. It was created using the included "TEMPLAT00.ROM" module.

-------------------------------------------------------------------------------------------------------------

Confirmed Compatible Notebook Models (incomplete list):

�Toshiba Qosmio X505 Q860

-------------------------------------------------------------------------------------------------------------
Warnings/Disclaimers:

Please remember to take proper precautions when flashing your BIOS. Even if the image is safe, the flashing process can still fail. Make sure that (if applicable) your notebook if fully charged (100%) and that the power plug is properly connected and that the chance of the power going out is as minimal as possible. Do not interupt the flashing process. If possible, it is a good idea to have a working CRISIS recovery disk when messing with your BIOS. Use the flashing instructions for your particular computer brand. DO NOT DO ANYTHING UNNECESSARY!!! 

I take absolutely NO responsibility for ANY damages that may occur to you, your computer, your personal belongings, your neighbors' cat, or any other personnel as a direct or indirect result/consequence of your, or any one else's, use or misuse of any of the files that were in this archive. You assume full responsibility for any damages that may occur by choosing to flash, or otherwise use, this image or any of the other files within this archive.

Have fun! :)

-------------------------------------------------------------------------------------------------------------
If you have any questions, please feel free PM me on any of the sites listed below.


ALIAS; bios-mods.com: Sml6397 | MDL: ArcticFreeze | forum.notebookreview.com: StevenL

~Steven